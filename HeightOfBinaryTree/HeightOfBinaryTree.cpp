// HeightOfBinaryTree.cpp : Este archivo contiene la función "main". La ejecución del programa comienza y termina ahí.
//

#include <iostream>

using namespace std;

class Node
{
public:
    int data;
    Node* left;
    Node* right;
};

Node* NewNode(int data)
{
    Node* node = new Node();
    node->data = data;
    node->left = NULL;
    node->right = NULL;

    return(node);
}

static int Height(Node* x)
{
    if (x == NULL)
    {
        return 0;
    }

    return 1 + max(Height(x->left), Height(x->right));
}

int main()
{
    // First tree
    Node* x = NewNode(15);
    x->left = NewNode(10);
    x->right = NewNode(20);
    x->left->left = NewNode(8);
    x->left->right = NewNode(12);
    x->right->left = NewNode(16);
    x->right->right = NewNode(25);
    x->right->right->left = NewNode(35);
    x->right->right->left->right = NewNode(45);

    cout << "Height of the tree: " << Height(x) << endl;
}

// Ejecutar programa: Ctrl + F5 o menú Depurar > Iniciar sin depurar
// Depurar programa: F5 o menú Depurar > Iniciar depuración

// Sugerencias para primeros pasos: 1. Use la ventana del Explorador de soluciones para agregar y administrar archivos
//   2. Use la ventana de Team Explorer para conectar con el control de código fuente
//   3. Use la ventana de salida para ver la salida de compilación y otros mensajes
//   4. Use la ventana Lista de errores para ver los errores
//   5. Vaya a Proyecto > Agregar nuevo elemento para crear nuevos archivos de código, o a Proyecto > Agregar elemento existente para agregar archivos de código existentes al proyecto
//   6. En el futuro, para volver a abrir este proyecto, vaya a Archivo > Abrir > Proyecto y seleccione el archivo .sln
